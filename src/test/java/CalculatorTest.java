import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {

    @Test
    public void calculateTest1() {
        assertEquals(5, new Calculator().calculate(4, 1));
        assertEquals(5, new Calculator().calculate(1, 4));
    }

    @Test
    public void calculateTest2() {
        assertEquals(5, new Calculator().calculate(2, 3));
        assertEquals(5, new Calculator().calculate(3, 2));
    }
}